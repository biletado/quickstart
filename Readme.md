# Biletado Quickstart

Quickstart on how to implement your own API-Backend

## Install kind
> this is a separate documentation

```shell
kubectl cluster-info
```

## install biletado
```shell
kubectl create namespace biletado
kubectl config set-context --current --namespace biletado
kubectl apply -k https://gitlab.com/biletado/kustomize.git//overlays/kind?ref=main --prune -l app.kubernetes.io/part-of=biletado -n biletado
kubectl rollout status deployment -n biletado -l app.kubernetes.io/part-of=biletado --timeout=600s
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
```

See [biletado/kustomize](https://gitlab.com/biletado/kustomize) for more options and more details.

You shoud now be able to open biletado at [localhost:9090](http://localhost:9090) and its [API-docs](http://localhost:9090/rapidoc/)

## create a webservice
Like the example in this repository with [Express](https://expressjs.com/)

## run your webservice
For this example, use the IntelliJ run configuration.

## test the service
Open the API-docs, configure the server (Port 3000 in this example) and send the status-request.

## build it as a docker-container
Write your [`Containerfile`](./Containerfile) and build the project as a container image.

In this example, no external build is necessary: `buildah bud -t registry.gitlab.com/biletado/quickstart:local-dev`

## upload your image to your kind-node
To make your image available inside the cluster, you have to upload the image to all nodes.

See [biletado/kustomize](https://gitlab.com/biletado/kustomize) for more options and more details.

```shell
podman save registry.gitlab.com/biletado/quickstart:local-dev --format oci-archive -o quickstart.tar
kind load image-archive quickstart.tar -n kind-cluster
```

## test your image with a kustomization.yaml
```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - https://gitlab.com/biletado/kustomize.git//overlays/kind

patches:
  - patch: |-
      - op: replace
        path: /spec/template/spec/containers/0/image
        value: registry.gitlab.com/biletado/quickstart:local-dev
      - op: replace
        path: /spec/template/spec/containers/0/ports/0/containerPort
        value: 3000
      - op: replace
        path: /spec/template/spec/containers/0/imagePullPolicy
        value: Never
    target:
      group: apps
      version: v1
      kind: Deployment
      name: assets
```

```shell
# execute this in the folder with kustomization.yaml
kubectl apply -k . --prune -l app.kubernetes.io/part-of=biletado -n biletado
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
```

## test the running image
open the API-docs again and switch to the default server.
Try the status endpoint.

## setup a CI/CD pipeline
Build your image automatically, e.g. with a gitlab-ci and a local runner like described in [biletado/pipeline](https://gitlab.com/biletado/pipeline)

## implement configuration options
Think about your configuration, implement them and write a patch if necessary.
The only configuration this example has is the environment variable `EXPRESS_PORT`.

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - https://gitlab.com/biletado/kustomize.git//overlays/kind

patches:
  - patch: |-
      - op: replace
        path: /spec/template/spec/containers/0/image
        value: registry.gitlab.com/biletado/quickstart:local-dev
      - op: replace
        path: /spec/template/spec/containers/0/ports/0/containerPort
        value: 4000
      - op: replace
        path: /spec/template/spec/containers/0/imagePullPolicy
        value: Never
      - op: add
        path: /spec/template/spec/containers/0/env/-
        value:
          name: EXPRESS_PORT
          value: "4000"
    target:
      group: apps
      version: v1
      kind: Deployment
      name: assets
```

## connect to the database for local development
To connect to the database running in the cluster, `kubectl port-forward services/postgres 5432:5432` can be used.
This makes the database accessible on `localhost:5432` on you workstation.
Please note, that the service needs `postgres:5432` as host if it runs inside the cluster.

## write your final kustomization

Write the kustomization with loading the image from a registry.

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - https://gitlab.com/biletado/kustomize.git//overlays/kind

patches:
  - patch: |-
      - op: replace
        path: /spec/template/spec/containers/0/image
        value: registry.gitlab.com/biletado/quickstart:latest
      - op: replace
        path: /spec/template/spec/containers/0/ports/0/containerPort
        value: 3000
    target:
      group: apps
      version: v1
      kind: Deployment
      name: assets
```
