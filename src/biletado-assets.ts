import express from 'express';
import cors from 'cors';
const biletadoAssets = express();
const port = process.env.EXPRESS_PORT;

biletadoAssets.use(cors({origin: true}))

biletadoAssets.get('/api/v3/assets/status', (req, res) => {
  res.type('json');
  res.send({authors:["Matthias Blümel"]});
});

biletadoAssets.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});

process.on('SIGINT', () => {process.exit()});  // CTRL+C
process.on('SIGQUIT', () => {process.exit()}); // Keyboard quit
process.on('SIGTERM', () => {process.exit()}); // `kill` command
